#
# iX-SpecOps ZedSHELL

export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export ZSH=~/.oh-my-zsh
#export PATH="/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin"

# Add $HOME/bin, $GOPATH/bin, etc, as needed
# TODO: Check for go, create $HOME/bin if ! exist, check for composer, etc.
export GOPATH=~/gocode
export PATH="/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin:$HOME/bin:$HOME/.bin:$GOPATH/bin"
export SHELL=/usr/local/bin/zsh
export EDITOR=vim
export TERM=xterm-256color
export JAVA_HOME=/usr/local/openjdk8/
export JRE_HOME=/usr/local/openjdk8/jre
export LD_LIBRARY_PATH=/usr/local/openjdk8/jre/lib/amd64/jli
export IOCAGE_COLOR="TRUE"

# Needs most installed & alias for man pages
#export PAGER=most

# SOME THEMES WE LIKE
#ZSH_THEME="wedisagree"
#ZSH_THEME="xiong-chiamiov-plus"
#ZSH_THEME="trapd00r"
#ZSH_THEME="josh"
#ZSH_THEME="robbyrussel"
#ZSH_THEME="agnoster"
#ZSH_THEME="half-life"
#ZSH_THEME="dracula"

# DONT DEFAULT TO PL9k THEME: looks strange
# TODO: Install fonts for PL9k
## powerlevel9k customizing
ZSH_THEME="powerlevel9k/powerlevel9k"
POWERLEVEL9K_MODE='nerdfont-complete'
#POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(custom_internet_signal context os_icon load disk_usage dir rbenv vcs)
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context os_icon load disk_usage dir rbenv vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator background_jobs history time)
POWERLEVEL9K_OS_ICON_BACKGROUND=white
POWERLEVEL9K_OS_ICON_FOREGROUND=red
POWERLEVEL9K_OS_ICON_VISUAL_IDENTIFIER_COLOR=""
POWERLEVEL9K_LEFT_SEGMENT_SEPARATOR=$'\uE0B0'
#POWERLEVEL9K_LEFT_SEGMENT_SEPARATOR=$'\uE0C0'
# Segment in black and white
POWERLEVEL9K_LOAD_CRITICAL_BACKGROUND="black"
POWERLEVEL9K_LOAD_CRITICAL_FOREGROUND="white"
POWERLEVEL9K_LOAD_WARNING_BACKGROUND="black"
POWERLEVEL9K_LOAD_WARNING_FOREGROUND="white"
POWERLEVEL9K_LOAD_NORMAL_BACKGROUND="black"
POWERLEVEL9K_LOAD_NORMAL_FOREGROUND="white"
# Colorize only the visual identifier
POWERLEVEL9K_LOAD_CRITICAL_VISUAL_IDENTIFIER_COLOR="red"
POWERLEVEL9K_LOAD_WARNING_VISUAL_IDENTIFIER_COLOR="yellow"
POWERLEVEL9K_LOAD_NORMAL_VISUAL_IDENTIFIER_COLOR="green"
#POWERLEVEL9K_MODE='awesome-patched'
#POWERLEVEL9K_MODE='awesome-fontconfig'

CASE_SENSITIVE="true"
DISABLE_AUTO_UPDATE="true"
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"
HIST_STAMPS="mm.dd.yyyy"

plugins=(vi-mode,
  git,
  gitflow,
  github,
  colorize,
  wp-cli,
  history,
  history-substring-search,
  tmux,
  zsh-navigation-tools,
  terraform,
  composer,
  nomad,
  supervisor,
  tmux-cssh
  )

# SOME HANDY ALIASES
alias zcli="zerotier-cli"
alias artisan="php artisan"
alias zsource="source ~/.zshrc"
alias zsudo="sudo -E zsh"
alias clr="pygmentize -g"
alias clrz="colorize"
alias composer="composer --ansi"
alias view="vim -R"
alias wp="wp --allow-root"
#alias man="man -P most"

# IF ROOT, DO NOT SUDO THE COMMAND
if [[ ! $UID == 0 || ! $EUID == 0 ]]; then
        alias vm="sudo /usr/local/sbin/vm"
   # else
        # unalias vm
fi

alias virsh="sudo virsh"
alias mccpr="mc cp --recursive"
alias mcmirror="mc mirror --force --remove --watch --exclude $MINEXCLUDE "

# SORT ALIASES
alias lslarge="ls -ablhprtFT | sort -n +4"
alias lstime="ls -ablhprtFT"

# GRAB THE OHMYZSH INIT & NAV TOOLS
source $ZSH/oh-my-zsh.sh
source /usr/local/share/zsh-navigation-tools/zsh-navigation-tools.plugin.zsh

# Must be LAST
plugins=( /usr/home/matto/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting)
