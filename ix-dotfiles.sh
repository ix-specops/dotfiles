#!/bin/sh

#
# iX-SpecOps Dotfiles Installation Script
#

clear

#
# Check for $OSTYPE for pkg mgr
#
platform=`uname -s`
case "${platform}" in
  Linux*)   echo "Linux" && pkg_mgr='apt' ;;
  Darwin*)  echo "Mac OSX" ;; 
  Win*)     echo "Win" ;;
  BSD*)     echo "FreeBSD" && pkg_mgr='pkg' ;;
  *)        echo "unknown: ${platform}" ;;
esac

#
# COLORS! But only if connected to a terminal that supports it
#

  if which tput >/dev/null 2>&1; then
    if [ -e /usr/bin/tput ] && [ ! -e /usr/local/bin/tput ]; then
      sudo ${pkg_mgr} install -y ncurses
      sudo mv -f /usr/bin/tput /usr/bin/tput.orig
    fi
      ncolors=$(tput colors)
  fi

  if [ -t 1 ] && [ -n "$ncolors" ] && [ "$ncolors" -ge 8 ]; then
    RED="$(tput setaf 1)"
    GREEN="$(tput setaf 2)"
    YELLOW="$(tput setaf 3)"
    BLUE="$(tput setaf 4)"
    BOLD="$(tput bold)"
    NORMAL="$(tput sgr0)"
  else
    RED=""
    GREEN=""
    YELLOW=""
    BLUE=""
    BOLD=""
    NORMAL=""
  fi

#
# Onboard with a bit of flare ;)
#

printf "${BLUE}"
printf "_____________________________________________________\n"
echo "  ___  __    ____                   ___             " 
echo " (_) \/ /   / ___| _ __   ___  ___ / _ \ _ __  ___  "
echo " | |\  /____\___ \| '_ \ / _ \/ __| | | | '_ \/ __| "
echo " | |/  \_____|__) | |_) |  __/ (__| |_| | |_) \__ \ "
echo " |_/_/\_\   |____/| .__/ \___|\___|\___/| .__/|___/ "
echo "                  |_|                   |_|         "
printf "${RED}"
echo "                  _       _    __ _ _               "
echo "               __| | ___ | |_ / _(_) | ___  ___     "
echo "              / _  |/ _ \| __| |_| | |/ _ \/ __|    "
echo "             | (_| | (_) | |_|  _| | |  __/\__ \    "
echo "              \__,_|\___/ \__|_| |_|_|\___||___/    "
printf "_____________________________________________________\n\n"
sleep 2; # Add Flare
echo "                                                    "
printf "${YELLOW}          PREPARE TO BE ASSIMILATED.\n\n"
printf "_____________________________________________________\n\n"
printf "${NORMAL}"
sleep 2; # Add Flare

#
# Clone iX-Specops Dotfiles from Bitbucket to user dir
#

# Bail on Error
set -e

if [ -d "$HOME/dotfiles" ]; then
    mv $HOME/dotfiles $HOME/dotfiles-backup
fi

if [ ! -e /usr/local/bin/git ]; then
	sudo ${pkg_mgr} install -y git
fi
    echo "Cloning iX-SpecOps Dotfiles..."
    git clone --depth 1 https://bitbucket.org/ix-specops/dotfiles $HOME/dotfiles

#
# Initialize here
#

INFO="[$DATE-dotfiles]:"
DOTFILES="$HOME/.dotfiles"
dir=$(CDPATH= cd -- "$(dirname -- "$0")" && pwd)

#
# PKG Requirements 
#

if [ ! -e '/usr/bin/env vim' ]; then sudo ${pkg_mgr} install -y vim; fi

PKGINS="sudo ${pkg_mgr} install -y" # cmd var
pkgname="
    powerline-fonts
    py27-powerline-status
    tmux
    most
    go
    zsh
    rsync
    zsh-navigation-tools
    bash
    tmate
    ncdu
    git
    gotty
    colorize"

#
# Install packages
#

    printf "${BLUE}\nInstalling packages for dotfiles. ${NORMAL}\n"
    pkgs_count=$( echo $pkgname | wc -l )
    for package in $(jot $pkgs_count ); do $PKGINS $pkgname; done
#
# Some Linux-y symlinks
#

   printf "${BLUE}\nCreating symlinks for bash & python. ${NORMAL}\n"
    sudo ln -nfs /usr/local/bin/bash /usr/sbin/bash
    sudo ln -nfs /usr/local/bin/python2.7 /usr/sbin/python2
    sudo ln -nfs /usr/local/bin/python2.7 /usr/sbin/python

#
# BEHOLD: A Chrome tab-crash workaround! YAY
#

#TODO: Check for Chrome & md entry in fstab
    printf "${BLUE}
        Add this Chrome tab-crash workaround to /etc/fstab. ${NORMAL}\n"
    printf "${YELLOW}    md ${HOME}/.cache/chromium mfs rw,late,-w${USER}:wheel,-s300m 2 0${NORMAL}\n"
# echo "md ${HOME}/.cache/chromium mfs rw,late,-w${USER}:wheel,-s300m 2 0" | sudo tee -a /etc/fstab
sleep 5

#
# Sane permissions on this CR#@P1!
#

  umask g-w,o-w

#
# List & Backup all the dirs&files
#

printf "${BLUE}\n Backing up current files to $HOME/dfile-bups. ${NORMAL}\n"
 H=${HOME}
 dirname="
        .vim
        .oh-my-zsh
        .zsh
        .tmux
        "
 filename="
        .vimrc
        .zshrc
        .tmux.conf
        "

       if [ ! -f $HOME/dfile-bups ]; then
        mkdir -p $HOME/dfile-bups
       fi

        for f in $dirname; do mv "$f" "$H/dfile-bups/$f-bak"; mkdir -p "$f"; done
        for f in $filename; do mv "$f" "$H/dfile-bups/$f-bak"; done

#
# Copy in all the good stuff from the repo
#
  rsync -arh $HOME/dotfiles/template/ $HOME

#
# Untar fonts & move to /usr/local/share/fonts & run fc-cache
#

  printf "${RED} COPYING FONTS TO /usr/local/share/fonts${NORMAL}"
  tar zxvf $HOME/dotfiles/p9fonts.tgz
  sudo rsync -avhr $HOME/dotfiles/fonts/ /usr/local/share/fonts
  sudo fc-cache -s

  tar zxvf $HOME/dotfiles/tmux.tgz
  sudo rsync -avhr $HOME/dotfiles/.tmux/ $HOME/.tmux
#
# Git clone VUNDLE
#

  printf "${BLUE}Cloning Vundle the VIM pkg manager...${NORMAL}\n"
 # git clone https://github.com/gmarik/Vundle.vim $HOME/.vim/bundle/Vundle.vim

#
# Change USER shell to zsh
#

  printf "${BLUE}Time to change your default shell to zsh!${NORMAL}\n"
  sudo pw user mod ${USER} -s /usr/local/bin/zsh
  sleep 2

#
# Install all the VIM plugins
#

  vim -c "execute \"PluginInstall\" | qa"

#
# PHEW! We are finished. Fire it up & run away ;)
#

  env zsh

